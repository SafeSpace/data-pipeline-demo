variable "aws_region" {
  default = "us-east-2"
}

variable "cluster-name" {
  default = "data-pipeline-demo"
  type    = string
}
