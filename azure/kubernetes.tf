# provider "kubernetes" {
#   config_path = "../kubeconfig"
# }

provider kubernetes {
  host                   = azurerm_kubernetes_cluster.cluster.kube_config.0.host
  client_certificate     = base64decode(azurerm_kubernetes_cluster.cluster.kube_config.0.client_certificate)
  client_key             = base64decode(azurerm_kubernetes_cluster.cluster.kube_config.0.client_key)
  cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.cluster.kube_config.0.cluster_ca_certificate)

}
resource "kubernetes_config_map" "dag-requirements" {
  metadata {
    name = "requirements"
  }

  data = {
    "requirements.txt" = file("${path.module}/requirements.txt")
  }
}

# resource "kubernetes_secret" "airflow" {
#   metadata {
#     name = "airflow"
#   }

#   data = {
#     airflow-fernetKey = "eGZ4S1FqWVNmU2xHbGhpTWpGWGRMSHJmSXZhUG92Sk0="
#     airflow-password = "airflow"
#   }

#   type = "opaque"
# }

# resource "kubernetes_secret" "postgres" {
#   metadata {
#     name = "postgres"
#   }

#   data = {
#     postgresql-password = "gYQs0SB62s"
#     postgresql-postgres-password = "0nJsOlaq3p"
#   }

#   type = "opaque"
# }

# resource "kubernetes_secret" "redis-password" {
#   metadata {
#     name = "redis-password"
#   }

#   data = {
#     redis-password = "ZLCva6CQe3"
#   }

#   type = "opaque"
# }