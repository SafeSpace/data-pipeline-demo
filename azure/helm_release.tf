# provider "helm" {
#   kubernetes {
#     config_path = "../kubeconfig"
#   }
# }

provider "helm" {
  kubernetes {
    host                   = azurerm_kubernetes_cluster.cluster.kube_config.0.host
    client_certificate     = base64decode(azurerm_kubernetes_cluster.cluster.kube_config.0.client_certificate)
    client_key             = base64decode(azurerm_kubernetes_cluster.cluster.kube_config.0.client_key)
    cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.cluster.kube_config.0.cluster_ca_certificate)

  }
}
resource "helm_release" "airflow" {
  name       = "airflow"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "airflow"
  version    = "10.0.4"

  values = [
    file("${path.module}/airflow-values.yaml")
  ]
}

resource "helm_release" "mongodb" {
  name       = "mongodb"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "mongodb"
  version    = "10.15.2"

  values = [
    file("${path.module}/mongodb-values.yaml")
  ]
}