terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.48.0"
    }
  }
}
provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg" {
  name     = "data-pipeline-demo"
  location = "eastus2"
}

resource "azurerm_kubernetes_cluster" "cluster" {
  name                = "data-pipeline-demo"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  dns_prefix          = "data-pipeline-demo"

  default_node_pool {
    name       = "default"
    node_count = "2"
    vm_size    = "Standard_D2s_v3"
  }

  identity {
    type = "SystemAssigned"
  }
}